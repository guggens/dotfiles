#!/bin/bash

function install_python {
    pip3 install pipenv --user
}

function install_packages {
    apt update
    apt install -y \
        silversearcher-ag \
        jq \
        python3 \
        python3-pip \
        python3-dev \
        python3-setuptools \
        vim \
	    zsh \
	    cifs-utils \
	    docker.io \
	    ntp \
	    rng-tools \
	    gnupg
    wget https://github.com/sharkdp/bat/releases/download/v0.9.0/bat_0.9.0_amd64.deb
    dpkg -i bat_0.9.0_amd64.deb
    rm bat_0.9.0_amd64.deb
    post_setup
}

function install_terraform {
    wget https://releases.hashicorp.com/terraform/0.12.4/terraform_0.12.4_linux_amd64.zip
    unzip -o terraform_0.12.4_linux_amd64.zip -d ~/.local/bin
}

function post_setup {
    install_python
    install_terraform
    chsh -s /usr/bin/zsh
}

if apt --version &> /dev/null; then
    install_packages
fi
