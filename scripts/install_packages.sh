apt update
apt install -y \
    curl \
    git \
    jq \
    vim \
    zsh \
    cifs-utils \
    docker.io \
    ntp
