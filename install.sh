#!/bin/bash
cd ~
rm -rf .dotfiles
git clone https://gitlab.com/guggens/dotfiles.git --depth 1 ~/.dotfiles
(
    cd .dotfiles
    ./install
)
