#!/bin/zsh

if [[ $commands[jq] ]]; then
    alias jq="jq -C"
fi

if [[ $commands[bat] ]]; then
    alias cat="PAGER=less bat -p"
fi

alias gitprivate="git config --global user.email 'guggens@gmail.com'"
alias gitwork="git config --global user.email 'sascha.guggenberger@daimler.com'"
alias synctime="sudo date -s \"$(curl -sD - google.com | grep '^Date:' | cut -d' ' -f3-6)Z\""
alias mountshare="sudo mount -t cifs -o username='goebels',uid=$(id -u),gid=$(id -g),forceuid,forcegid,rw,nounix,file_mode=0777,dir_mode=0777,vers=3.0,nodfs //s415001f.detss.corpintra.net/E415_Projekte/smartservices /mnt/smartservices_share"
alias mfa="source ~/.aws/aws_mfa.sh"
alias upgrade="sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y"
alias update=upgrade



