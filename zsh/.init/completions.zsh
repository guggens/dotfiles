if [[ $commands[terraform] ]]; then
    autoload -U +X bashcompinit && bashcompinit
    complete -o nospace -C $(which terraform) terraform
fi

# source /usr/bin/aws_zsh_completer.sh
