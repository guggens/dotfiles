# Dotfiles

## Short install

`bash -c "$(curl -fsSL https://gitlab.com/guggens/dotfiles/raw/master/install.sh)"`

## Long install

clone the repository:

`git clone --depth=1 https://gitlab.com/guggens/dotfiles.git $HOME/.dotfiles`

and run `install.sh` from modules directories you wish to install

## About

Dotfiles repositories are great practice among developers, because we can share the way how we maintain creating stable cross-platform setups. This dotfile repository contains few modules where each one has got it's own README explaining what is kept inside VCS and why. Here is the short summary of them:

* git - global gitignore and git config integrated with external tools (icdiff, pycharm). Also some useful aliases
* zsh - zsh integrated with zplug and oh-my-zsh with many external binaries to speed up daily work of DevOps

## Additional files

* .hooks - git hooks used to generate configuration files of this repository
* lib - shared shell code which helps in autoinstallation of configurations

